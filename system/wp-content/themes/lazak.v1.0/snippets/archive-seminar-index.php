<?php
    $fData = get_posts(array(
        'posts_per_page'   => -1,
        'meta_query' => array(
            array(
                'key' => 's_date_for_query',
                'value' =>date("Y-m-d"),
                'compare' => '>=',
                'type'=>'DATE'
            ),
        ),
        'orderby' => 'meta_value',
        'meta_key' => 's_date_for_query',
        'order' => 'ASC',
        'post_type'  => 'seminar'
    ));

    $pData = get_posts(array(
        'posts_per_page'   => 3,
        'meta_query' => array(
            array(
                'key' => 's_date_for_query',
                'value' =>date("Y-m-d"),
                'compare' => '<',
                'type'=>'DATE'
            )
        ),
        'orderby' => 'meta_value',
        'meta_key' => 's_date_for_query',
        'order' => 'DESC',
        'post_type'  => 'seminar'
    ));

    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>行事予定</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/seminar/tit01.jpg" alt="行事予定"></h1>

                        <div id="c02">
                            <div class="row">
                                <div class="col-12">
                                    <section class="west pt-3">
                                        <h2 class="tit_style04">今後の予定</h2>
                                        <?php if(!empty($fData)) : ?>
                                            <table class="table_style02 t01_01 mt-3 mb-5">
                                                <thead>
                                                    <tr>
                                                        <th>カテゴリ</th>
                                                        <th>開催日</th>
                                                        <th>タイトル</th>
                                                        <th>場所</th>
                                                        <th>内容</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($fData as $post): setup_postdata($post); ?>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    $terms = get_the_terms( $post->ID, 'seminar_category' );
                                                                    if ( $terms && ! is_wp_error( $terms ) ) :

                                                                    $draught_links = array();

                                                                    foreach ( $terms as $term ) {
                                                                        $draught_links[] = $term->name;
                                                                    }

                                                                    $on_draught = join( ", ", $draught_links );
                                                                ?>
                                                                <?php echo $on_draught; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><?php the_field('s_date'); ?></td>
                                                            <td><?php the_title(); ?></td>
                                                            <td><?php the_field('s_place'); ?></td>
                                                            <td><?php the_field('s_theme'); ?></td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        <?php else : ?>
                                            <div class="no_seminar pb-5">開催予定の行事予定はありません。</div>
                                        <?php endif; ?>
                                    </section>
                                </div>
                                <div class="col-12">
                                    <section class="east">
                                        <h2 class="tit_style03">過去の行事</h2>

                                        <?php if(!empty($pData)) : ?>

                                            <table class="table_style02 t01_02 mt-3 mb-5">
                                                <thead>
                                                    <tr>
                                                        <th>カテゴリ</th>
                                                        <th>開催日</th>
                                                        <th>タイトル</th>
                                                        <th>場所</th>
                                                        <th>内容</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($pData as $post): setup_postdata($post); ?>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    $terms = get_the_terms( $post->ID, 'seminar_category' );
                                                                    if ( $terms && ! is_wp_error( $terms ) ) :

                                                                    $draught_links = array();

                                                                    foreach ( $terms as $term ) {
                                                                        $draught_links[] = $term->name;
                                                                    }

                                                                    $on_draught = join( ", ", $draught_links );
                                                                ?>
                                                                <?php echo $on_draught; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><?php the_field('s_date'); ?></td>
                                                            <td><?php the_title(); ?></td>
                                                            <td><?php the_field('s_place'); ?></td>
                                                            <td><?php the_field('s_theme'); ?></td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>

                                        <?php endif; ?>
                                    </section>
                                </div>
                                <div class="col-12">
                                    <div class="btn_wrap">
                                        <div class="btn01">
                                            <a href="<?php echo esc_url(home_url('/seminar/?params=all')); ?>">行事予定一覧</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>行事予定</h2>
                            <ul>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/seminar/category/seminar/')); ?>">
                                        <span>学習会</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/seminar/category/etc/')); ?>">
                                        <span>その他</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>