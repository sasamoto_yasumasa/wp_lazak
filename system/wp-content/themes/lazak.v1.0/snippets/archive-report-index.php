<?php
    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <?php if(is_date()) : ?>
                                <li><a href="<?php echo esc_url(home_url('/report/')); ?>">活動報告</a></li>
                                <li><?php echo get_the_date('Y年'); ?>の活動報告</li>
                             <?php else: ?>
                                <li>活動報告</li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <?php if(is_date()) : ?>
                            <h1><?php echo get_the_date('Y年'); ?>の活動報告</h1>
                        <?php else: ?>
                            <h1>
                                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/report/tit01.jpg" alt="活動報告">
                            </h1>
                        <?php endif; ?>

                        <div id="c01">
                               <?php if(is_date()) : ?>
                                    <h2 class="tit_style01"><?php echo get_the_date('Y年'); ?>の活動</h2>
                                <?php else: ?>
                                    <h2 class="tit_style01">最新の活動</h2>
                                <?php endif; ?>
                            <?php if ( have_posts() ) :  ?>
                                <table class="report_style02">
                                    <tbody>
                                        <?php while ( have_posts() ) : the_post(); ?>
                                            <tr>
                                                <td><span class="<?php echo esc_html(la_get_term_slug()); ?>"><?php echo esc_html(la_get_term()); ?></span></td>
                                                <?php if(!empty(get_the_content())) : ?>
                                                    <td>
                                                        <a href="<?php the_permalink(); ?>"><?php the_field('report02'); ?></a>
                                                        <?php if(have_rows('report01')): ?>
                                                            <ul class="ico11">
                                                                <?php while(have_rows('report01')): the_row(); ?>
                                                                    <li><a href="<?php echo wp_get_attachment_url(get_sub_field('report01_01')); ?>" target="_blank"><span class="btn01"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn01.png" alt="PDFダウンロード"></span><span class="t01"><?php the_sub_field('report01_02'); ?></span></a></li>
                                                                <?php endwhile; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php else : ?>
                                                    <td>
                                                        <?php the_field('report02'); ?>
                                                        <?php if(have_rows('report01')): ?>
                                                            <ul class="ico11">
                                                                <?php while(have_rows('report01')): the_row(); ?>
                                                                    <li><a href="<?php echo wp_get_attachment_url(get_sub_field('report01_01')); ?>" target="_blank"><span class="btn01"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn01.png" alt="PDFダウンロード"></span><span class="t01"><?php the_sub_field('report01_02'); ?></span></a></li>
                                                                <?php endwhile; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php endif ; ?>
                                            </tr>
                                        <?php endwhile ; ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <div>現在活動報告はございません。</div>
                            <?php endif ; ?>
                            <div class="pagination">
                                <?php wp_pagenavi(); ?>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>活動報告</h2>
                            <ul>
                                <?php wp_get_archives( 'type=yearly&post_type=report' ); ?>
                            </ul>
                        </aside>
                        <div class="side_report_pdf_btn">
                            <a href="<?php echo esc_url(home_url('/report/?params=pdf')); ?>"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/report/btn01.png" alt="意見書・声明文（PDF）一覧"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
?>
