<?php

  /**
   * preタグを含めてvar_dumpする。
   *
   * @param mixed $input
   * @return string
   */
    function d($input){
      echo '<pre style="text-align:left;">';
      var_dump($input);
      echo '</pre>';
    }


  /**
   * 投稿が属しているタームを一つ返す。複数ある場合は使用しない。
   *
   * @return string
   */
    function la_get_term($post_id =''){
      global $post;

      if(!$post_id){
          $post_id = $post->id;
      }

      // 投稿 ID から投稿オブジェクトを取得
      $postObj = get_post( $post_id );
      // その投稿から投稿タイプを取得
      $post_type = $postObj->post_type;
      // その投稿タイプからタクソノミーを取得
      $taxonomies = get_object_taxonomies( $post_type, 'objects' );

      $ret = '';

      foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
        // 投稿に付けられたタームを取得
        $terms = get_the_terms( $postObj->ID, $taxonomy_slug );
        if ( !empty( $terms ) ) {
          foreach ( $terms as $term ) {
              $ret = $term->name;
          }
        }
      }
      return $ret;

    }

  /**
   * 投稿tが属しているタームをリンクを含めて一つ返す。複数ある場合は使用しない。
   *
   * @return string
   */
    function la_get_term_link($post_id =''){

      global $post;

      if(!$post_id){
          $post_id = $post->id;
      }

      // 投稿 ID から投稿オブジェクトを取得
      $postObj = get_post( $post_id );
      // その投稿から投稿タイプを取得
      $post_type = $postObj->post_type;
      // その投稿タイプからタクソノミーを取得
      $taxonomies = get_object_taxonomies( $post_type, 'objects' );

      $ret = '';


      $cat_slug = '';

      foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
          if($taxonomy->hierarchical){
            $cat_slug = $taxonomy_slug;
            break;
          }
      }

      if($cat_slug != ''){
        // 投稿に付けられたタームを取得
        $terms = get_the_terms( $postObj->ID, $cat_slug );
        if ( !empty( $terms ) ) {
          foreach ( $terms as $term ) {
             $ret = '<a href="'. get_term_link( $term->slug, $taxonomy_slug ) . '">' .$term->name . '</a>';
          }
        }
      }


      return $ret;
    }


  /**
   * 投稿が属しているタームをリンクを含めて一つ返す。複数ある場合は使用しない。
   *
   * @return string
   */
    function la_get_tag_link($post_id =''){

      global $post;

      // 投稿 ID から投稿オブジェクトを取得
      if(!$post_id){
          $post_id = $post->id;
      }

      $postObj = get_post( $post_id );
      // その投稿から投稿タイプを取得
      $post_type = $postObj->post_type;
      // その投稿タイプからタクソノミーを取得
      $taxonomies = get_object_taxonomies( $post_type, 'objects' );

      $ret = '';


      $tag_slug = '';

      foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
          if(!$taxonomy->hierarchical){
            $tag_slug = $taxonomy_slug;
            break;
          }
      }

      if($tag_slug != ''){
        // 投稿に付けられたタームを取得
        $terms = get_the_terms( $postObj->ID, $tag_slug );
        if ( !empty( $terms ) ) {
          foreach ( $terms as $term ) {
             $ret .= '<a href="'. get_term_link( $term->slug, $taxonomy_slug ) . '">' .$term->name . '</a>';
          }
        }
      }


      return $ret;
    }


  /**
   * 投稿が属しているタームのスラッグを一つ返す。複数ある場合は使用しない。
   *
   * @return string
   */
    function la_get_term_slug(){
      global $post;
      // 投稿 ID から投稿オブジェクトを取得
      $postObj = get_post( $post->ID );
      // その投稿から投稿タイプを取得
      $post_type = $postObj->post_type;
      // その投稿タイプからタクソノミーを取得
      $taxonomies = get_object_taxonomies( $post_type, 'objects' );

      $ret = '';

      foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
        // 投稿に付けられたタームを取得
        $terms = get_the_terms( $postObj->ID, $taxonomy_slug );
        if ( !empty( $terms ) ) {
          foreach ( $terms as $term ) {
              $ret = $term->slug;
          }
        }
      }

      return $ret;

    }

    /**
    * 投稿が属しているタームをリンクを含めて一つ返す。複数ある場合は使用しない。
    *
    * @return string
    */
    function la_get_relationship_term_id($params){

        global $post;

        $params = array_merge(
            array(
              'tax_name' => '',
            ),
            (array)$params
        );

        $ret = array();
        $term_id = '';

        if(is_single()){
            $termObj = get_the_terms($post->ID,$params['tax_name']);
            $term_id = $termObj[0]->term_id;
        }elseif(is_tax()){
            $termObj = get_term_by('name', single_term_title('', false), $params['tax_name']);
            $term_id = $termObj->term_id;
        }

        $ancestors = get_ancestors( $term_id, $params['tax_name']);
        $ret = array_reverse($ancestors);
        $ret[] = $term_id;

        return $ret;

    }


    function la_get_term_breadcrumb($params){

        $ret = array();

        $params = array_merge(
            array(
              'term_ids' => '',
              'tax_name' => '',
            ),
            (array)$params
        );

        $length = count($params['term_ids']);
        $count = 0;

        if($params['term_ids']){
            foreach ($params['term_ids'] as $k => $v) {
                if(is_single() || ++$count !== $length){
                    $tLink = get_term_link( $v, $params['tax_name'] );
                    $ret[] = '<li><a href="' . $tLink . '">';
                    $ret[] = esc_html(get_term($v)->name);
                    $ret[] = '</a></li>';
                }else{
                    $ret[] = '<li>';
                    $ret[] = esc_html(get_term($v)->name);
                    $ret[] = '</li>';
                }
            }
        }

        return implode ('', $ret );

    }

  /**
   * acf imageタグを出力
   *
   * @param array $params
   * @return string
   */
    function la_acf_get_image($params){

        $params = array_merge(
            array(
              'a_id' => '',
              'size' => 'full',
              'attr' => array(),
            ),
            (array)$params
        );

        $ret = array();

        $a_id = $params['a_id'];
        if($a_id){
            $setImage='';
            $setImage= wp_get_attachment_image_src($a_id,$params['size']);
            $ret[] = '<img src="' . $setImage[0] . '"';
            foreach ($params['attr'] as $k => $v) {
              $ret[] = ' ' . $k . '="' . $v . '"';
            }
            $ret[] = ' />';
        }

      return implode ('', $ret );

    }

  /**
   * acf imageタグを出力
   *
   * @param array $params
   * @return string
   */
    function la_acf_get_image_lightbox($params){

        $params = array_merge(
            array(
              'a_id' => '',
              'size' => 'full',
              'thumbnail_size' => 'full',
              'attr' => array(),
              'a_attr' => array(),
            ),
            (array)$params
        );

        $ret = array();

        $a_id = $params['a_id'];
        if($a_id){
            $setImage='';
            $setImage= wp_get_attachment_image_src($a_id,$params['size']);
            $setImage2='';
            $setImage2= wp_get_attachment_image_src($a_id,$params['thumbnail_size']);
            $ret[] = '<a href="' . $setImage[0] . '"';
            foreach ($params['a_attr'] as $k => $v) {
              $ret[] = ' ' . $k . '="' . $v . '"';
            }
            $ret[] = ' >';
            $ret[] = '<img src="' . $setImage2[0] . '"';
            foreach ($params['attr'] as $k => $v) {
              $ret[] = ' ' . $k . '="' . $v . '"';
            }
            $ret[] = ' />';
            $ret[] = ' </a>';
        }

      return implode ('', $ret );

    }

  /**
   * acf imageタグを出力
   *
   * @param array $params
   * @return string
   */
    function la_acf_get_svg($params){

        $params = array_merge(
            array(
              'a_id' => '',
              'size' => 'full',
              'attr' => array(),
            ),
            (array)$params
        );

        $ret = array();

        $a_id = $params['a_id'];
        if($a_id){
            $setImage='';
            $setImage= wp_get_attachment_image_src($a_id,$params['size']);
            $ret[] = '<object data="' . $setImage[0] . '" type="image/svg+xml"';
            foreach ($params['attr'] as $k => $v) {
              $ret[] = ' ' . $k . '="' . $v . '"';
            }
            $ret[] = ' />';
            $ret[] = '</object>';
        }

      return implode ('', $ret );

    }


  /**
   * acf リンクタグを出力
   *
   * @param array $params
   * @return string
   */
    function la_acf_get_link( $params ){

        $params = array_merge(
            array(
              'url' => '/',
              'target' => false,
              'label' => '',
            ),
            (array)$params
        );

      $ret = array();

      $ret[] = '<a href="';
      $ret[] = $params['url'];
      $ret[] = '"';
      if($params['target']){
        $ret[] = ' target="_blank"';
      }
      $ret[] = '>';
      $ret[] = $params['label'];
      $ret[] = '</a>';

      return implode ('', $ret );

    }


  /**
   * acf imageタグを出力
   *
   * @param array $params
   * @return string
   */
    function la_acf_get_file_pdf($params){

        $params = array_merge(
            array(
              'a_id' => '',
              'attr' => array(),
            ),
            (array)$params
        );

        $ret = array();

        $a_id = $params['a_id'];
        if($a_id){
            $setfile='';
            $setfile= wp_get_attachment_url($a_id);
            $ret[] = '<a target="_blank" href="' . $setfile . '"';
            foreach ($params['attr'] as $k => $v) {
              $ret[] = ' ' . $k . '="' . $v . '"';
            }
            $ret[] = ' >';
            $ret[] = '<img src="';
            $ret[] = get_template_directory_uri();
            $ret[] = '/images/common/icn_pdf01.png" alt="PDF">';
            $ret[] = '<a>';
        }
        return implode ('', $ret );
    }


  /**
   * acf imageタグを出力
   *
   * @param array $params
   * @return string
   */
    function la_acf_get_file_dxf($params){

        $params = array_merge(
            array(
              'a_id' => '',
              'attr' => array(),
            ),
            (array)$params
        );

        $ret = array();

        $a_id = $params['a_id'];
        if($a_id){
            $setfile='';
            $setfile= wp_get_attachment_url($a_id);
            $ret[] = '<a href="' . $setfile . '"';
            foreach ($params['attr'] as $k => $v) {
              $ret[] = ' ' . $k . '="' . $v . '"';
            }
            $ret[] = ' >';
            $ret[] = '<img src="';
            $ret[] = get_template_directory_uri();
            $ret[] = '/images/common/icn_dxf01.png" alt="DXF">';
            $ret[] = '<a>';
        }
        return implode ('', $ret );
    }

    function la_get_post_date($format,$post_id=''){

        global $post;

        if(!$post_id){
          $post_id = $post->id;
        }

        $postObj = get_post( $post_id );

        $dt = new DateTime($postObj->post_date);

        return  $dt->format($format);

    }
