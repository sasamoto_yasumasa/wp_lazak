<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138688810-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-138688810-1');
    </script>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
    <meta name="description" content="在日コリアン弁護士協会のLAZAK">
    <meta name="keywords" content="LAZAK,在日コリアン弁護士協会">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
    <link rel="shortcut icon" href="<?php echo esc_url(home_url('/')); ?>favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo esc_url(home_url('/')); ?>apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo esc_url(home_url('/')); ?>android-chrome-256x256.png">
    <?php wp_head(); ?>
</head>
<body id="<?php get_body_id(true); ?>">
    <header id="header">
        <div class="inner">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-4">
<!--                         <div id="hd_lang">
                            <ul class="clearfix">
                                <li class="current"><a href="">Japanese</a></li>
                                <li><a href="">Korean</a></li>
                                <li><a href="">English</a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div id="ci01" class="col-3">
                        <h1><a href="<?php echo esc_url(home_url('/')); ?>" ><img src="<?php echo get_template_directory_uri(); ?>/images/common/hd_logo01.png" alt="LAZAK 在日コリアン弁護士協会" width="220" height="51"></a></h1>
                    </div>
                    <div class="col-3">
                        <div id="hd_link">
                            <ul class="clearfix">
                                <li id="hd-link01"><a href="<?php echo esc_url(home_url('/link/')); ?>">リンク集</a></li>
                                <li id="hd-link02"><a href="<?php echo esc_url(home_url('/contact/')); ?>">お問合せ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <!-- /#header --></header>
    <nav id="gnav">
        <div class="inner">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/about_us/')); ?>">LAZAKについて</a></li>
                            <li><a href="<?php echo esc_url(home_url('/activity/')); ?>">LAZAKの取組み</a></li>
                            <li><a href="<?php echo esc_url(home_url('/report/')); ?>">活動報告</a></li>
                            <li><a href="<?php echo esc_url(home_url('/seminar/')); ?>">行事予定</a></li>
                            <li><a href="<?php echo esc_url(home_url('/book/')); ?>">書籍・出版物</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="wrap">