<?php

    $allflg = false;
    if(isset($_GET['params'])){
        if($_GET['params'] === 'all'){
           $allflg = true;
        }
    }

    if($allflg){
        include_once( __DIR__ . '/snippets/archive-seminar-all.php');
    }else{
        include_once( __DIR__ . '/snippets/archive-seminar-index.php');
    }
