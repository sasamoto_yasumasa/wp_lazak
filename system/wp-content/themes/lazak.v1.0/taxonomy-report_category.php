<?php
    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/report/')); ?>">活動報告</a></li>
                            <li><?php echo single_term_title(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1><?php echo single_term_title(); ?>の活動報告</h1>
                        <div id="c01">
                            <h2 class="tit_style01"><?php echo single_term_title(); ?>の活動</h2>
                            <?php if ( have_posts() ) :  ?>
                                <table class="report_style02">
                                    <tbody>
                                        <?php while ( have_posts() ) : the_post(); ?>
                                            <tr>
                                                <td><span class="<?php echo esc_html(la_get_term_slug()); ?>"><?php echo esc_html(la_get_term()); ?></span></td>
                                                <td><?php echo get_the_date('Y/m/d'); ?></td>
                                                <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
                                            </tr>
                                        <?php endwhile ; ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <div>現在活動報告はございません。</div>
                            <?php endif ; ?>
                            <div class="pagination">
                                <?php wp_pagenavi(); ?>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>活動報告</h2>
                            <ul>
                                <?php wp_get_archives( 'type=yearly&post_type=report' ); ?>
                            </ul>
                        </aside>
                        <div class="side_report_pdf_btn">
                            <a href="<?php echo esc_url(home_url('/report/?params=pdf')); ?>">意見書・声明文（PDF）一覧</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
?>
