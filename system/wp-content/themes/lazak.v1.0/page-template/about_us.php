<?php
/*
 * Template Name: LAZAKについて
 */
    get_header();
?>
    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>LAZAKについて</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about_us/tit01.jpg" alt="LAZAKについて"></h1>
                        <div id="c01">
                            <div class="lead_text">
                                <div class="block01 clearfix">
                                    <div class="img01"><img src="<?php echo get_template_directory_uri(); ?>/images/about_us/c01_img01.jpg" alt=""></div>
                                    <h2>在日コリアン弁護士協会は、2001年5月に設立された在日コリアン弁護士及び司法修習生が参加する団体です。</h2>
                                    <p>英語で <b>Lawyers’Association of ZAINICHI Korean</b> と表記し、<b>LAZAK（ラザック）</b>と略します。</p>
                                    <p>在日コリアン弁護士協会（LAZAK）は、２００１年５月、東京において、在日コリアン法律家協会として２８名の原始会員により設立され、翌２００２年６月に在日コリアン弁護士協会への組織改編を経て、日本各地の在日コリアン弁護士及び司法修習生が参加しています。</p>
                                <!-- /.block01 --></div>
                                <div class="block02 pt-4">
                                    <h3 class="tit_style05">団体名の理由</h3>
                                    <p>団体名が在日コリアン弁護士協会とされたのは、次のような理由です。</p>
                                    <p>まず、民族分断状況によって、在日同胞社会にも長く南北の政治的対立が影響し、その結果「在日韓国人」「在日朝鮮人」という二つの呼称が用いられ、またこのような在日同胞の分断状況を克服するため「在日韓国・朝鮮人」という呼称も生まれました。<br>一方、近年は毎年約１万人の同胞が日本国籍を取得している事実もあります。</p>
                                    <p>私達は、このような歴史と現実を前提に、自らのエスニシティーをコリアであると考える全ての在日同胞弁護士の結集体として、最近広く用いられるようになった「在日コリアン」の呼称を選択しました。英語表記中に敢えて、「KOREAN in JAPAN」ではなく、「ZAINICHI KOREAN」の文字を用いたのは、在日同胞の国籍、言語、文化、習慣等が多様化し、「在日コリアン」と呼ぶのが最も相応しいエスニック集団となっている状況を考慮した結果です。</p>
                                    <p>現に、会員の中には、自らの姓名の発音についても、韓国語を用いる者も日本語を用いる者もいますし、また、自らのエスニシティーをコリアと考えながら日本式の姓名を名乗る者もいます。 </p>
                                <!-- /.block02 --></div>
                            </div>
                            <div class="local_nav">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="item">
                                            <a href="<?php echo esc_url(home_url('/about_us/message/')); ?>">
                                                <dl>
                                                    <dt>代表挨拶</dt>
                                                    <dd>LAZAK代表からのメッセージをご覧いただけます。</dd>
                                                </dl>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="item">
                                            <a href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">
                                                <dl>
                                                    <dt>設立の契機</dt>
                                                    <dd>設立の契機をご確認いただけます。</dd>
                                                </dl>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="item">
                                            <a href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">
                                                <dl>
                                                    <dt>設立趣意書</dt>
                                                    <dd>設立趣意書をご覧いただけます。</dd>
                                                </dl>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="item">
                                            <a href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">
                                                <dl>
                                                    <dt>役員紹介</dt>
                                                    <dd>役員をご紹介いたします。</dd>
                                                </dl>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>LAZAKについて</h2>
                            <ul>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/message/')); ?>">
                                        <span>代表挨拶</span>
                                        </dl>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">
                                        <span>設立の契機</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">
                                        <span>設立趣意書</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">
                                        <span>役員紹介</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>