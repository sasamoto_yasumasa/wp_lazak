<?php
/*
 * Template Name: サイトマップ
 */
    get_header();
?>

    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>サイトマップ</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/sitemap/tit01.jpg" alt="サイトマップ"></h1>
                        <div id="c01">
                            <ul>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/')); ?>">HOME</a>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-4">
                                            <a class="link_style01" href="<?php echo esc_url(home_url('/about_us/')); ?>">LAZAKについて</a>
                                        </div>
                                        <div class="col-4">
                                            <div class="childlink">
                                                <a class="link_style01" href="<?php echo esc_url(home_url('/about_us/message/')); ?>">代表挨拶</a>
                                                <a class="link_style01" href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">設立の契機</a>
                                                <a class="link_style01" href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">設立趣意書</a>
                                                <a class="link_style01" href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">役員紹介</a>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/activity/')); ?>">LAZAKの取組み</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/report/')); ?>">活動報告</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/seminar/')); ?>">行事予定</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/book/')); ?>">書籍・出版物</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/link/')); ?>">リンク集</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/contact/')); ?>">お問合せ</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/privacy/')); ?>">プライバシーポリシー</a>
                                </li>
                                <li>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/sitemap/')); ?>">サイトマップ</a>
                                </li>
                            </ul>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>