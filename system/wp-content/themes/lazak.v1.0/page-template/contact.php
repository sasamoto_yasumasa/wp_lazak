<?php
/*
 * Template Name: お問合せ
 */

session_start();

mb_language('Japanese');
mb_internal_encoding('UTF-8');

include ( dirname(__FILE__) . '/../include/utility.php');

$postNameArr = array("username","username_furigana","tel1","mail1","mail2","message");
common_strip_slashes_post($postNameArr);

//モードが設定されていない、又は編集モードなら入力フォーム表示
if(!isset($_POST['mode']) || $_POST['mode'] == 'edit'){
  viewForm();
  exit;
}

//確認画面
if($_POST['mode']=="check"){

  $checkArr = run_validate_inquiry();

  if($checkArr["isError"]){
    viewForm();
    exit;
  }
  $setHidden = createHidden();
  $ticket = $_SESSION['ticket']=md5(uniqid().mt_rand());
  viewConfirm();
  exit;
}

//完了画面
if($_POST['mode']=="send"){
  //ワンタイムチェック
  if(!isset($_POST['ticket']) || $_POST['ticket'] !== $_SESSION['ticket']){
    unsetPOST();
    delSession();
    viewSessionError();
    exit;
  }
  sendFormMail();
  delSession();
  exit;
}


//フォーム表示
function viewForm(){
  get_header();
  include ( dirname(__FILE__) . '/../include/contact-index.php');
  get_footer();

}

//確認画面表示
function viewConfirm(){
  get_header();
  include ( dirname(__FILE__) . '/../include/contact-confirm.php');
  get_footer();
}

//セッションエラー表示
function viewSessionError(){
  get_header();
  include ( dirname(__FILE__) . '/../include/contact-session_error.php');
  get_footer();
}

//メール送信
function sendFormMail(){

  require_once (  dirname(__FILE__) . '/../include/qdmail.php7.php');
  require_once (  dirname(__FILE__) . '/../include/qdsmtp.php7.php');

$adm_text = <<<EOD
お問合せが届きました。
お問合せ内容は、下記の通りです。

_%data%_

EOD;

$user_text = <<<EOD
_%name%_ 様

この度は、お問合せを頂き誠に有難うございます。
追って、担当者からご連絡差し上げます。連絡がない場合はシステムの不具合等の可能性がございます。
その際はご容赦ください。

このメールは、配信専用のアドレスで配信されています。
このメールに返信されても、返信内容の確認およびご返答ができません。 あらかじめご了承ください。

LAZAK 在日コリアン弁護士協会

EOD;

  $dammy_address = "sys@lazak.jp";//フォーム送信元アドレス
  $system_from = "お問合せフォーム" ;//管理者向けメール送信者名
  //$mail_address_inquiry = array('maeda@lab-art.co.jp');//フォーム送信先メールアドレス
  //$mail_address_inquiry = array('sasamoto@lab-art.co.jp');//フォーム送信先メールアドレス
  $mail_address_inquiry = array('lazak_20020720@yahoo.co.jp');//フォーム送信先メールアドレス
  $toName =array('Lazak');
  $mail_from_inquiry = "LAZAK";//ユーザー向けメール送信者名
  $mail_title_inquiry = "LAZAKへのお問合せ有難うございます。";//ユーザー向け件名
  $mail_body_inquiry = $user_text;
  $mail_title_adm_inquiry = "LAZAKに問合せがありました";//管理者向け件名
  $mail_body_adm_inquiry = $adm_text;

  $data="";
  $data .= "【お名前】".$_POST['username']."\n";
  $data .= "【お名前（フリガナ）】".$_POST['username_furigana']."\n";
  $data .= "【電話番号】".$_POST['tel1']."\n";
  $data .= "【メールアドレス】".$_POST['mail1']."\n";
  $data .= "【備考】\n". str_replace("<br />","", nl2br($_POST['message'])) . "\n";


  $mailbody_adm= preg_replace('/_%data%_/',$data, $mail_body_adm_inquiry);
  $mailbody_adm= preg_replace('/_%name%_/',$_POST['username'], $mailbody_adm);
  $mailbody_adm = preg_replace('/(_%.+?%_)/', '',$mailbody_adm);

  $mailbody_user= preg_replace('/_%data%_/',$data, $mail_body_inquiry);
  $mailbody_user= preg_replace('/_%name%_/',$_POST['username'], $mailbody_user);
  $mailbody_user = preg_replace('/(_%.+?%_)/', '',$mailbody_user);


  $mail = new Qdmail();

  $mail -> to( $_POST['mail1'] );
  $mail -> subject( $mail_title_inquiry );
  $mail -> text( $mailbody_user );
  $mail -> from($dammy_address,$mail_from_inquiry);
  $mail ->send();


  $mail2 = new Qdmail();

  $mail2 -> to($mail_address_inquiry,$toName,true);
  $mail2 -> subject($mail_title_adm_inquiry );
  $mail2 -> text( $mailbody_adm );
  $mail2 -> from('sys@lazak.jp',$system_from);
  $mail2 ->send();

  unsetPOST();

  get_header();
  include ( dirname(__FILE__) . '/../include/contact-finish.php');
  get_footer();
}
