<?php
/*
 * Template Name: 代表挨拶
 */
    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/about_us/')); ?>">LAZAKについて</a></li>
                            <li>代表挨拶</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1 class="p_title01"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about_us/message/tit01.jpg" alt="代表挨拶"></h1>
                        <div id="c01">
                            <div class="lead_text">
                                <p>在日コリアン弁護士協会（LAZAK）は、設立から17年経過し、活動のテーマは人権問題から企業法務まで、また、国内の会員相互の親睦から海外の弁護士との交流や国際会議への参加など、幅広いものとなりました。会員も130名を超え、日本で生まれ育ったいわゆる在日コリアン（両親や祖父母に日本人など他の国籍の人がいるハーフやクォーターも含みます）二世、三世から、韓国で生まれ育った者など多様化しています。</p>
                                <p>そのため、LAZAKが日本で活動するコリアン弁護士の団体として何をするのか、何をすべきか、という問いについて議論が生じるときもあります。</p>
                                <p>それでも、私たちがこの団体に参加し、活動しているのは、日本社会で生きながら韓国・朝鮮とつながっているという思い、日本社会で弁護士をしている私たちだからこそできる活動があるのだという自負があるからだと思います。</p>
                                <p>今、在日コリアンに対する誤解や無理解に基づく偏見や差別は強まり、国連の繰り返される勧告にもかかわらず、私たちを取り巻く状況は良くなっているとは言い難い状況にあります。本邦外出身者に対する不当な差別的言動の解消に向けた取組の推進に関する法律（ヘイトスピーチ解消法）が施行された現在でもヘイトスピーチはなくならず、むしろインターネット上では増えています。国や多くの地方自治体は、外国人の公務員への就任を極端に制限しています。憲法上、少数者の人権保障のための最後の砦とされている最高裁判所も、法律に規定がないにもかかわらず、ある時期から外国人の調停委員就任を拒否しています。近時は、コリアン系と推測されやすい氏名の弁護士は、それだけで理不尽な大量の懲戒請求を受けるようになりました。</p>
                                <p>しかし、このような不合理な差別・嫌がらせを受けているのは在日コリアンだけではありません。対話のない一方的なインターネット上の誹謗・中傷によって、あるいは議論を避け、少数者の意見を聞かない権力者によって作られた施策によって、あるいは企業や私的団体内における理不尽な命令や嫌がらせによって、自分の尊厳を否定されていると感じたり、同じ人間であるはずなのに格差や分断を感じている人は少なくないのではないでしょうか。</p>
                                <p>理想論と笑われるかもしれませんが、私は、一人一人がアイデンティティを大切にし、相互に同じ人間として理解し合い、その人格を認め、尊重し合えれば、差別はなくなり、みなが住みやすい社会になると思い、そのために活動しています。LAZAKとしても、団体としてのアイデンティティを大切にしながら、在日コリアンをはじめマイノリティが住みやすい社会は、すべての人にとって住みやすい社会になると信じ、そのために、法律家として言うべきことは言いつつ、理解・対話のための扉は開き、マイノリティを含むすべての人が住みやすい社会になるよう、活動したいと思っています。</p>
                                <p class="text-right">2018年8月 LAZAK代表 姜文江（Fumie Kyo）</p>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>LAZAKについて</h2>
                            <ul>
                                <li class="current">
                                    <a href="<?php echo esc_url(home_url('/about_us/message/')); ?>">
                                        <span>代表挨拶</span>
                                        </dl>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">
                                        <span>設立の契機</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">
                                        <span>設立趣意書</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">
                                        <span>役員紹介</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>