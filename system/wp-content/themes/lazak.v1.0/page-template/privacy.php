<?php
/*
 * Template Name: プライバシーポリシー
 */
    get_header();
?>

    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>プライバシーポリシー</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/privacy/tit01.jpg" alt="プライバシーポリシー"></h1>
                        <div id="c01">
                            <div class="lead_text">
                                <p>在日コリアン弁護士協会（以下「当協会」といいます）は、個人情報の取扱に関し、個人情報の保護に関する法律(以下｢個人情報保護法｣といいます)、その他個人情報の取扱について定められた関係法令を遵守するとともに、以下のプライバシーポリシーを定め、その適切な取扱に努めます。</p>
                            </div>
                            <section id="c01_01">
                                <h2 class="tit_style01">1. 個人情報の取得</h2>
                                <div class="summary">
                                    <p>当協会は、個人情報を利用目的の達成に必要な範囲で、適正な手段により取得します。</p>
                                </div>
                            </section>
                            <section id="c01_02">
                                <h2 class="tit_style01">2. 個人情報の利用目的</h2>
                                <div class="summary">
                                    <p>当協会は、個人情報を、以下の目的で利用します。<br>
                                    個人情報保護法その他の法令により認められる事由がある場合を除き、ご本人の同意がない限り、この範囲を超えて個人情報を利用することはありません。</p>
                                    <ul class="list_style04">
                                        <li>当協会の業務の適切かつ円滑な遂行</li>
                                        <li>その他、上記の利用目的に付随する事項の遂行</li>
                                    </ul>
                                </div>
                            </section>
                            <section id="c01_03">
                                <h2 class="tit_style01">3. 個人情報の第三者提供</h2>
                                <div class="summary">
                                    <p>当協会は、個人情報の第三者提供については、個人情報保護法、その他の法令を遵守します。</p>
                                </div>
                            </section>
                            <section id="c01_04">
                                <h2 class="tit_style01">4. 個人情報の管理</h2>
                                <div class="summary">
                                    <p>当協会は、個人情報の第三者提供については、個人情報保護法、その他の法令を遵守します。</p>
                                </div>
                            </section>
                            <section id="c01_05">
                                <h2 class="tit_style01">5. 個人情報の開示、訂正等のお求め</h2>
                                <div class="summary">
                                    <p>当協会は、個人情報について、個人情報保護法に基づく開示、訂正等ご本人からのお申出があった場合には、個人情報保護法に従い、適切に対応いたします。<br>
                                    詳しくは、以下のお問合せ先までご連絡ください。</p>
                                </div>
                            </section>
                            <section id="c01_05">
                                <h2 class="tit_style01">6. お問合せ窓口</h2>
                                <div class="summary">
                                    <p>当協会の個人情報の取扱に関するご意見、ご質問、開示等のご請求、その他個人情報の取扱に関するお問合わせの窓口は、当協会といたします。</p>
                                </div>
                            </section>
                            <section id="c01_05">
                                <h2 class="tit_style01">7. 本プライバシーポリシーの継続的改善</h2>
                                <div class="summary">
                                    <p>当協会は、個人情報の取扱に関する運用状況を適宜見直すとともに、継続的な改善に努めます。必要に応じて、プライバシーポリシーを変更することがあります。</p>
                                </div>
                            </section>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>