<?php
/*
 * Template Name: 設立の契機
 */
    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/about_us/')); ?>">LAZAKについて</a></li>
                            <li>設立の契機</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1 class="p_title01"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about_us/timing/tit01.jpg" alt="設立の契機"></h1>
                        <div id="c01">
                            <div class="lead_text">
                                <h2 class="tit_style05">設立の契機について</h2>
                                <p>設立の直接の契機は，２００１年に日本の保守派の国会議員らによって新しい日本国籍取得法案が検討され始めたことに遡ります。</p>
                                <p>当時，定住外国人の地方選挙権法案が日本国会において審議されていたところ，この法案に反対する国会議員らが，現行の帰化制度より簡易な手続での日本国籍取得法案を提唱することで，定住外国人の選挙権法案の必要性を大幅に低下させようと図りました。</p>
                                <p>これに対して在日コリアン弁護士２２名の有志が声明を発表して，地方参政権法案を葬り去ろうとする意図に反対するとともに，新たな日本国籍取得法案を作成するなら，当事者である在日コリアンの意思を的確に反映させるべきことを主張し，また，植民地支配の清算の一環として，在日コリアンに対する人権保障を広い範囲で実現すべきことを訴えました。</p>
                                <p>この声明を発表した２２名の弁護士が，在日コリアン弁護士団体の必要性に合意し，自身らが母体となって，LAZAK設立に至りました。</p>
                                <p>LAZAKは，設立の際，</p>
                                <ul class="num">
                                    <li>在日コリアンにおける「法の支配」の実現</li>
                                    <li>あらゆるマイノリティの権利自由の擁護</li>
                                    <li>全ての在日コリアン法律家の結集</li>
                                    <li>世界のコリアンとの連帯</li>
                                </ul>
                                <p>を，その目的として謳っています。これらの目的に沿って，LAZAKは，設立後，学者・知識人らを招いたシンポジュウムの開催，在日コリアンの人権に関わる裁判の支援，在日コリアンに関する書籍出版，海外のコリアン弁護士との交流等の活動を行っています。 </p>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>LAZAKについて</h2>
                            <ul>
                                <li >
                                    <a href="<?php echo esc_url(home_url('/about_us/message/')); ?>">
                                        <span>代表挨拶</span>
                                        </dl>
                                    </a>
                                </li>
                                <li class="current">
                                    <a href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">
                                        <span>設立の契機</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">
                                        <span>設立趣意書</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">
                                        <span>役員紹介</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>