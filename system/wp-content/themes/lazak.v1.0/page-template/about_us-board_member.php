<?php
/*
 * Template Name: 役員紹介
 */
    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/about_us/')); ?>">LAZAKについて</a></li>
                            <li>役員紹介</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1 class="p_title01"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/about_us/board_member/tit01.jpg" alt="役員紹介"></h1>
                        <div id="c01">
                            <section id="c01_01">
                                <h2 class="tit_style01">代表</h2>
                                <section class="profile">
                                    <h3><span>姜文江</span><small>（Fumie Kyo）</small></h3>
                                    <ul>
                                        <li>神奈川県弁護士会</li>
                                    </ul>
                                </section>
                            </section>
                            <section id="c01_02">
                                <h2 class="tit_style01">副代表</h2>
                                <section class="profile">
                                    <h3><span>金哲敏</span><small>（Cholmin Kim）</small></h3>
                                    <ul>
                                        <li>東京弁護士会</li>
                                    </ul>
                                </section>
                                <section class="profile">
                                    <h3><span>韓雅之</span><small>（Masayuki Han）</small></h3>
                                    <ul>
                                        <li></li>
                                        <li>大阪弁護士会</li>
                                    </ul>
                                </section>
                            </section>
                            <div class="sign text-right">
                                <p>（2018年度・2019年度）</p>
                            </div>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>LAZAKについて</h2>
                            <ul>
                                <li >
                                    <a href="<?php echo esc_url(home_url('/about_us/message/')); ?>">
                                        <span>代表挨拶</span>
                                        </dl>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">
                                        <span>設立の契機</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">
                                        <span>設立趣意書</span>
                                    </a>
                                </li>
                                <li class="current">
                                    <a href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">
                                        <span>役員紹介</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>