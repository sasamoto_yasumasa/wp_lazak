<?php
/*
 * Template Name: LAZAKの取組み
 */
    get_header();
?>

    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>LAZAKの取組み</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/activity/tit01.jpg" alt="LAZAKの取組み"></h1>
                        <div id="c01">
                            <section id="c01_04">
                                <h2 class="tit_style01">1. 訴訟支援など</h2>
                                <div class="summary">
                                    <p>LAZAKは、団体として、在日コリアンの人権に関わる訴訟に対し支援を行っています。<br>以前に訴訟支援を行った訴訟は次のとおりです。</p>
                                    <ul class="l01 mt-4">
                                        <li>・在日外国人無年金訴訟</li>
                                        <li>・高槻マイノリティ教育権訴訟</li>
                                        <li>・在日コリアン弁護士入居差別訴訟</li>
                                    </ul>
                                    <p>LAZAKの会員には、有志として、在日コリアンの人権に関わる訴訟に代理人として参加している者もいます。</p>
                                </div>
                            </section>
                            <section id="c01_05">
                                <h2 class="tit_style01">2. 意見書、声明の発表</h2>
                                <div class="summary">
                                    <p>在日コリアンをはじめ、日本に居住するマイノリティの人権を擁護し、人種差別撤廃、他民族・多文化共生の観点から、意見書、声明を発表しています。</p>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/report/?params=pdf')); ?>">過去の意見書、声明はこちら</a>
                                </div>
                            </section>
                            <section id="c01_06">
                                <h2 class="tit_style01">3. シンポジウムの開催</h2>
                                <div class="summary">
                                    <p>在日コリアンに関する人権課題や法律問題をテーマに、市民の方を対象としたシンポジウムを開催しています。</p>
                                </div>
                            </section>
                            <section id="c01_01">
                                <h2 class="tit_style01">4. 書籍の出版</h2>
                                <div class="summary">
                                    <p>在日コリアンに関わる人権、法律などに関する書籍を出版しています。</p>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/book/')); ?>">書籍・出版物の詳細ページヘ</a>
                                </div>
                            </section>
                            <section id="c01_03">
                                <h2 class="tit_style01">5. 外国人司法委員・調停委員問題</h2>
                                <div class="summary">
                                    <p>「外国人司法委員・調停委員問題」とは、日本国籍のない弁護士が、その所属弁護士会から、簡易裁判所の司法委員及び家庭裁判所の調停委員に推薦されたにもかかわらず、裁判所から日本国籍がないことを理由に就任を拒絶されている、という問題です。現在、10数名のLAZAK会員も弁護士会から推薦されていますが、裁判所は就任を認めていません。</p>
                                    <p>日弁連や各弁護士会でこの問題に対して取り組んでいますが、LAZAK会員らもその取組みに参加しています。</p>
                                </div>
                            </section>
                            <section id="c01_07">
                                <h2 class="tit_style01">6. 国際的な人権活動、世界の法律家との交流</h2>
                                <div class="summary">
                                    <p>国連人種差別撤廃委員会や自由権規約委員会等にレポートを提出し、会員を派遣しています。また、日韓バーリーダーズ会議、世界韓人弁護士会（IAKL）に会員を派遣し、大韓弁護士協会や韓国の各地方弁護士会を訪問するなど、海外の法律家と国際的な交流を図っています。</p>
                                </div>
                            </section>
                            <section id="c01_02">
                                <h2 class="tit_style01">7. 会員間の連携・親睦、会内行事の開催</h2>
                                <div class="summary">
                                    <p>連絡網を整備し、メーリングリスト等を活用して、会員間での情報共有や意見交換を活発に行なっています。また、総会や拡大理事会では全国の在日コリアン弁護士が一堂に会しています。</p>
                                    <p>さらに、関東・関西の各地区で、在日コリアン弁護士及び修習生等が集まり、学習会を継続的に行っています。在日コリアン弁護士として必要と思われる知識・技術等について研鑽を積むとともに、会員相互の親睦を深めています。</p>
                                    <a class="link_style01" href="<?php echo esc_url(home_url('/seminar/')); ?>">行事予定の詳細ページヘ</a>
                                </div>
                            </section>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
 ?>