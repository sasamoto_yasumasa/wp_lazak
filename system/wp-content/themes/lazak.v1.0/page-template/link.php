<?php
/*
 * Template Name: リンク集
 */
    get_header();
?>

    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>リンク集</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/link/tit01.jpg" alt="リンク集"></h1>
                        <div id="c01">
                            <section id="c01_01">
                                <h2 class="tit_style01">弁護士団体</h2>
                                <ul class="list_style03">
                                    <li>
                                        <a target="_blank" href="http://www.nichibenren.or.jp/">
                                            日本弁護士連合会 ［<span class="link_style05">http://www.nichibenren.or.jp/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://koreanbar.or.kr/">
                                            大韓弁護士協会（韓国語・英語） ［<span class="link_style05">http://koreanbar.or.kr/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://www.iakl.net/">
                                            世界韓人弁護士会（韓国語・英語） ［<span class="link_style05">http://www.iakl.net/</span> ］
                                        </a>
                                    </li>
                                </ul>
                            </section>
                            <section id="c01_02">
                                <h2 class="tit_style01">会員のホームページ</h2>
                                <ul class="list_style03">
                                    <li>
                                        <a target="_blank" href="http://www.legal.ne.jp/">
                                            弁護士法人オルビス ［<span class="link_style05">http://www.legal.ne.jp/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://maya-law-kobe.jp/">
                                            まや法律事務所 ［<span class="link_style05">https://maya-law-kobe.jp/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://www.law-lim.com/">
                                            一心法律事務所 ［<span class="link_style05">http://www.law-lim.com/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://www.myh-law.com/">
                                            森岡・山本・韓 法律事務所 ［<span class="link_style05">https://www.myh-law.com/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://www.osaka-futaba.com/">
                                            大阪ふたば法律事務所 ［<span class="link_style05">http://www.osaka-futaba.com/</span> ］
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://www.mklo.org/">
                                            武蔵小杉合同法律事務所 ［<span class="link_style05">http://www.mklo.org/</span> ］
                                        </a>
                                    </li>
                                </ul>
                            </section>

                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
<?php
    get_footer();
 ?>