<?php
    $bData = get_posts(array(
        'posts_per_page'   => 1,
        'post_type'  => 'book'
    ));
    $rData = get_posts(array(
        'posts_per_page'   => 5,
        'post_type'  => 'report'
    ));
    $sData = get_posts(array(
        'posts_per_page'   => 3,
        'meta_query' => array(
            array(
                'key' => 's_date_for_query',
                'value' =>date("Y-m-d"),
                'compare' => '>=',
                'type'=>'DATE'
            ),
        ),
        'orderby' => 'meta_value',
        'meta_key' => 's_date_for_query',
        'order' => 'ASC',
        'post_type'  => 'seminar'
    ));


get_header(); ?>

    <div id="mv">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="item">
                        <div class="inner">
                            <h2><img src="<?php echo get_template_directory_uri(); ?>/images/home/mv_tex01.png" alt="在日コリアン、他の民族的少数者、ひいては、すべてのマイノリティの「個人の尊厳」が尊重される社会を目指して"></h2>
                            <a href="<?php echo esc_url(home_url('/about_us/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/home/mv_btn01.png" alt="LAZAKについて"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <article class="activity">
                            <h2 class="tit_style02">
                                <div class="row justify-content-between">
                                    <div class="col-3">
                                        <div class="txt">活動報告</div>
                                    </div>
                                    <div class="col-2">
                                        <div class="link"><a href="<?php echo esc_url(home_url('/report/')); ?>">一覧を見る</a></div>
                                    </div>
                                </div>
                            </h2>
                            <?php if(!empty($rData)) : ?>
                                <table class="report_style01">
                                    <tbody>
                                        <tr>
                                        <?php foreach($rData as $post): setup_postdata($post); ?>
                                            <tr>
                                                <td><span class="<?php echo esc_html(la_get_term_slug()); ?>"><?php echo esc_html(la_get_term()); ?></span></td>
                                                <td><?php echo get_the_date('Y/m/d'); ?></td>
                                                <?php if(!empty(get_the_content())) : ?>
                                                    <td>
                                                        <a href="<?php the_permalink(); ?>"><?php the_field('report02'); ?></a>
                                                        <?php if(have_rows('report01')): ?>
                                                            <ul class="ico11">
                                                                <?php while(have_rows('report01')): the_row(); ?>
                                                                    <li><a href="<?php echo wp_get_attachment_url(get_sub_field('report01_01')); ?>" target="_blank"><span class="btn01"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn01.png" alt="PDFダウンロード"></span><span class="t01"><?php the_sub_field('report01_02'); ?></span></a></li>
                                                                <?php endwhile; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php else :  ?>
                                                    <td>
                                                        <?php the_field('report02'); ?>
                                                        <?php if(have_rows('report01')): ?>
                                                            <ul class="ico11">
                                                                <?php while(have_rows('report01')): the_row(); ?>
                                                                    <li><a href="<?php echo wp_get_attachment_url(get_sub_field('report01_01')); ?>" target="_blank"><span class="btn01"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn01.png" alt="PDFダウンロード"></span><span class="t01"><?php the_sub_field('report01_02'); ?></span></a></li>
                                                                <?php endwhile; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php endif ;  ?>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                        </article>
                        <article class="study">
                            <h2 class="tit_style02">
                                <div class="row justify-content-between">
                                    <div class="col-3">
                                        <div class="txt">行事予定</div>
                                    </div>
                                    <div class="col-2">
                                        <div class="link"><a href="<?php echo esc_url(home_url('/seminar/')); ?>">詳細を見る</a></div>
                                    </div>
                                </div>
                            </h2>
                            <div class="row">
                                <div class="col-12">
                                    <section class="west">
                                        <?php if(!empty($sData)) : ?>
                                            <table class="table_style02 mt-3">
                                                <thead>
                                                    <tr>
                                                        <th>カテゴリ</th>
                                                        <th>開催日</th>
                                                        <th>タイトル</th>
                                                        <th>場所</th>
                                                        <th>内容</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($sData as $post): setup_postdata($post); ?>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    $terms = get_the_terms( $post->ID, 'seminar_category' );
                                                                    if ( $terms && ! is_wp_error( $terms ) ) :

                                                                    $draught_links = array();

                                                                    foreach ( $terms as $term ) {
                                                                        $draught_links[] = $term->name;
                                                                    }

                                                                    $on_draught = join( ", ", $draught_links );
                                                                ?>
                                                                <?php echo $on_draught; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><?php the_field('s_date'); ?></td>
                                                            <td><?php the_title(); ?></td>
                                                            <td><?php the_field('s_place'); ?></td>
                                                            <td><?php the_field('s_theme'); ?></td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        <?php else : ?>
                                            <div class="no_seminar pt-3 pb-3">開催予定の行事予定はありません。</div>
                                        <?php endif; ?>
                                    </section>
                                </div>
                        </article>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <div id="bnr_area01" class="bnr_area">
                            <ul>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/activity/')); ?>">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/common/side_bnr01.png" alt="LAZAKの取組み">
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <?php if(!empty($bData)) : ?>
                            <aside id="pickup_book">
                                <h2>Pickup Book</h2>
                                <?php foreach($bData as $post): setup_postdata($post); ?>
                                        <div class="inner">
                                            <h3><?php the_title(); ?><?php if(get_field('book01')): ?><br><?php the_field('book01'); ?><?php endif; ?></h3>
                                            <div class="row">
                                                <div class="col-4">
                                                    <?php if(get_field('book05')) : ?>
                                                        <?php
                                                            $params = array(
                                                              'a_id' => get_field('book05'),
                                                              'attr' => array(
                                                                'class' => 'img-fluid'
                                                              ),
                                                            );
                                                            echo la_acf_get_image($params);
                                                         ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-8">
                                                    <?php if(get_field('book02')): ?>
                                                        <p><?php the_field('book02'); ?></p>
                                                    <?php endif; ?>
                                                    <a href="<?php echo esc_url(home_url('/book/')); ?>#b<?php echo $post->ID; ?>">詳細を見る</a>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach; ?>
                            </aside>
                        <?php endif ; ?>


                        <div id="bnr_area02" class="bnr_area">
                            <ul>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/book/')); ?>">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/common/side_bnr02.png" alt="書籍のご紹介">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url(home_url('/link/')); ?>">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/common/side_bnr03.png" alt="リンク集">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();?>