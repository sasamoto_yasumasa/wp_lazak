<?php
global $post;
if(empty($post->post_content) && !get_field('report01',$post->ID)){
    wp_redirect(home_url(),301);
    exit;
}

get_header();
?>

<?php if(have_posts()): while(have_posts()):the_post(); ?>
    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/report/')); ?>">活動報告</a></li>
                            <li><?php echo get_the_title(); ?> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1><?php the_field('report02'); ?></h1>
                        <div id="c01">
                            <table class="report_style03">
                                <tbody>
                                    <tr>
                                        <td><span class="<?php echo esc_html(la_get_term_slug()); ?>"><?php echo esc_html(la_get_term()); ?></span></td>
                                        <td><?php echo get_the_date('Y/m/d'); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php if(get_the_content()) : ?>
                                <div class="content"><?php the_content(); ?></div>
                            <?php endif; ?>
                            <?php if(have_rows('report01')): ?>
                                <div class="attachments">
                                    <h5>ダウンロード</h5>
                                    <ul>
                                        <?php while(have_rows('report01')): the_row(); ?>
                                            <li><a href="<?php echo wp_get_attachment_url(get_sub_field('report01_01')); ?>" target="_blank"><?php the_sub_field('report01_02'); ?></a></li>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>活動報告</h2>
                            <ul>
                                <?php wp_get_archives( 'type=yearly&post_type=report' ); ?>
                            </ul>
                        </div>
                        <div class="side_report_pdf_btn">
                            <a href="<?php echo esc_url(home_url('/report/?params=pdf')); ?>"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/report/btn01.png" alt="意見書・声明文（PDF）一覧"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; endif; ?>

<?php
    get_footer();
?>
