$(function() {

    // ページの上部に戻る
    var pagetop = $('#pagetop');
    pagetop.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            pagetop.fadeIn();
        } else {
            pagetop.fadeOut();
        }
    });
    pagetop.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 300);
        return false;
    });

    $("#to_confirm_btn").on('click',function(){
        //console.log('te');
        $(this).closest('form').submit();
    });

    $("#to_index_btn").on('click',function(){
        //console.log('te');
        $('#edit_form').submit();
    });

    $("#to_send_btn").on('click',function(){
        //console.log('te');
        $('#send_form').submit();
    });

})