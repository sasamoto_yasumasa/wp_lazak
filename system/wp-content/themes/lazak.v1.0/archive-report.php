<?php 
    $pdfflg = false;
    if(isset($_GET['params'])){
        if($_GET['params'] === 'pdf'){
           $pdfflg = true;
        }
    }

    if($pdfflg){
        include_once( __DIR__ . '/snippets/archive-report-pdf.php');
    }else{
        include_once( __DIR__ . '/snippets/archive-report-index.php');
    }
