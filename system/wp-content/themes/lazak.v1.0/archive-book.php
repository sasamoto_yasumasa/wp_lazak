<?php
    get_header();
?>


    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>書籍・出版物</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1>
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/book/tit01.jpg" alt="書籍・出版物">
                        </h1>
                        <div id="c01">
                            <?php if ( have_posts() ) :  ?>
                                 <?php while ( have_posts() ) : the_post(); ?>
                                    <section id="b<?php echo $post->ID; ?>">
                                        <div class="inner">
                                            <div class="row">
                                                <div class="col-4">
                                                    <?php if(get_field('book05')) : ?>
                                                        <?php
                                                            $params = array(
                                                              'a_id' => get_field('book05'),
                                                              'attr' => array(
                                                                'class' => 'img-fluid'
                                                              ),
                                                            );
                                                            echo la_acf_get_image($params);
                                                         ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-8">
                                                    <h2><?php the_title(); ?></h2>
                                                    <?php if(get_field('book01')): ?>
                                                        <h3><?php the_field('book01'); ?></h3>
                                                    <?php endif; ?>
                                                    <?php if(get_field('book02')): ?>
                                                        <div class="summary">
                                                            <p><?php the_field('book02'); ?></p>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php if(have_rows('book03')): ?>
                                                        <table>
                                                            <tbody>
                                                                <?php while(have_rows('book03')): the_row(); ?>
                                                                    <tr>
                                                                        <th><?php the_sub_field('book03_01'); ?></th>
                                                                        <td><?php the_sub_field('book03_02'); ?></td>
                                                                    </tr>
                                                                <?php endwhile; ?>
                                                            </tbody>
                                                        </table>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <?php if(get_field('book04')) : ?>
                                                    <div class="buy">
                                                        <a target="_blank" class="link_style03" href="<?php the_field('book04') ?>">ご購入はこちらから</a>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                <?php endwhile ; ?>
                            <?php else : ?>
                                <div>現在書籍・出版物はございません。</div>
                            <?php endif ; ?>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
<?php
    get_footer();
 ?>