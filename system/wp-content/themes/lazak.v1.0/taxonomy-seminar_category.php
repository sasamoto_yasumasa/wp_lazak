<?php

    $fData = get_posts(array(
        'posts_per_page'   => -1,
        'meta_query' => array(
            array(
                'key' => 's_date_for_query',
                'value' =>date("Y-m-d"),
                'compare' => '>=',
                'type'=>'DATE'
            ),
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'seminar_category',
                'field' => 'slug',
                'terms' => $term
            )
        ),
        'orderby' => 'meta_value',
        'meta_key' => 's_date_for_query',
        'order' => 'ASC',
        'post_type'  => 'seminar'
    ));
    $aData = get_past_seminar_data($term);
    get_header();
?>

    <div id="content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li><a href="<?php echo esc_url(home_url('/seminar/')); ?>">行事予定</a></li>
                            <li><?php echo single_term_title(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <main id="primary">
                        <h1>行事予定（<?php echo single_term_title(); ?>）</h1>
                        <div id="c01">
                            <section class="pt-3">
                                <h2 class="tit_style04">今後の予定</h2>
                                <?php if(!empty($fData)) : ?>
                                    <table class="table_style02 t01_01 mt-3 mb-5">
                                        <thead>
                                            <tr>
                                                <th>カテゴリ</th>
                                                <th>開催日</th>
                                                <th>タイトル</th>
                                                <th>場所</th>
                                                <th>内容</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($fData as $post): setup_postdata($post); ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                            $terms = get_the_terms( $post->ID, 'seminar_category' );
                                                            if ( $terms && ! is_wp_error( $terms ) ) :

                                                            $draught_links = array();

                                                            foreach ( $terms as $term ) {
                                                                $draught_links[] = $term->name;
                                                            }

                                                            $on_draught = join( ", ", $draught_links );
                                                        ?>
                                                        <?php echo $on_draught; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?php the_field('s_date'); ?></td>
                                                    <td><?php the_title(); ?></td>
                                                    <td><?php the_field('s_place'); ?></td>
                                                    <td><?php the_field('s_theme'); ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                <?php else : ?>
                                    <div class="no_seminar pb-5">開催予定の行事予定はありません。</div>
                                <?php endif; ?>
                            </section>

                            <?php if(!empty( $aData)) : ?>
                            <section>
                                 <h2 class="tit_style03">過去の行事</h2>
                                <?php foreach ($aData as $year => $lists) : ?>
                                    <section class="pb-5">
                                        <h2 class="tit_style06"><?php echo $year; ?>年</h2>
                                        <table class="table_style02 t01_02">
                                            <thead>
                                                <tr>
                                                    <th>カテゴリ</th>
                                                    <th>開催日</th>
                                                    <th>タイトル</th>
                                                    <th>場所</th>
                                                    <th>内容</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($lists as $list): ?>
                                                    <tr id="s<?php echo $list['id'] ?>">
                                                        <td>
                                                            <?php
                                                                $terms = get_the_terms( $post->ID, 'seminar_category' );
                                                                if ( $terms && ! is_wp_error( $terms ) ) :

                                                                $draught_links = array();

                                                                foreach ( $terms as $term ) {
                                                                    $draught_links[] = $term->name;
                                                                }

                                                                $on_draught = join( ", ", $draught_links );
                                                            ?>
                                                            <?php echo $on_draught; ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?php echo $list['date']; ?></td>
                                                        <td><?php echo $list['title']; ?></td>
                                                        <td><?php echo $list['place']; ?></td>
                                                        <td><?php echo nl2br ( $list['theme'] ); ?></td>
                                                    </tr>
                                                <?php endforeach ?>

                                            </tbody>
                                        </table>
                                    </section>
                                <?php endforeach; ?>
                            </section>
                            <?php endif; ?>


                            <div class="btn_wrap pt-5">
                                <div class="btn02">
                                    <a href="<?php echo esc_url(home_url('/seminar/')); ?>">行事予定TOPに戻る</a>
                                </div>
                            </div>

                        </div>
                    </main>
                </div>
                <div class="col-4">
                    <div class="secondly">
                        <aside class="side_local_nav">
                            <h2>行事予定</h2>
                            <ul>
                                <li <?php if(single_term_title("", false) === '学習会') : ?> class="current" <?php endif; ?>>
                                    <a href="<?php echo esc_url(home_url('/seminar/category/seminar/')); ?>">
                                        <span>学習会</span>
                                    </a>
                                </li>
                                <li <?php if(single_term_title("", false) === 'その他') : ?> class="current" <?php endif; ?>>
                                    <a href="<?php echo esc_url(home_url('/seminar/category/etc/')); ?>">
                                        <span>その他</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    get_footer();
?>
