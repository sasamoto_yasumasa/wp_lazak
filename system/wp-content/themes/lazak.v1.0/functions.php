<?php
require_once(TEMPLATEPATH . '/functions/la_functions.php');

show_admin_bar( false );

//add_image_size( 'thumbnail01', 600, 366, true );
/**
 * 絵文字スクリプト削除
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

 if ( function_exists( 'date_default_timezone_set' ) ){
    date_default_timezone_set( 'Asia/Tokyo' );
 }
/**
 * 各投稿タイプの一覧に表示する件数を設定
 *
 * @return void
 */

    function lazak_change_posts_per_page($wp_query) {
        if ( is_admin() || ! $wp_query->is_main_query() ) return;
        if ( $wp_query->is_post_type_archive('report') || $wp_query->is_tax('report_category')) {
            $wp_query->set( 'posts_per_page', -1 );
            return;
        }elseif($wp_query->is_post_type_archive('seminar') || $wp_query->is_tax('seminar_category')){
            $wp_query->set( 'posts_per_page', -1 );
            return;
        }elseif($wp_query->is_post_type_archive('book') ){
            $wp_query->set( 'posts_per_page', -1 );
            return;
        }
    }
    add_action( 'pre_get_posts', 'lazak_change_posts_per_page' );



/**
 * JS共通変数をheadに出力
 *
 * @return void
 */

    function lazak_common_scripts() {
    ?>
    <script>
        var directory_uri = '<?php echo get_template_directory_uri(); ?>';
    </script>
    <?php
    }
    add_action( 'wp_head', 'lazak_common_scripts', 1 );


/**
 * 念を付ける
 *
 * @return void
 */

    function add_nen_year_archives( $link_html ) {
        $regex = array (
            "/ title='([\d]{4})'/"  => " title='$1年'",
            "/ ([\d]{4}) /"         => " $1年 ",
            "/>([\d]{4})<\/a>/"        => "><span>$1年</span></a>"
        );
        $link_html = preg_replace( array_keys( $regex ), $regex, $link_html );
        return $link_html;
    }
    add_filter( 'get_archives_link', 'add_nen_year_archives' );

/**
 * CSS JSの出力コントロール
 *
 * @return void
 */

    function lazak_scripts_styles() {

        //既存jquery削除
        wp_deregister_script('jquery');

        //css読み込み
        wp_enqueue_style( 'font-awesome.all.css', '//use.fontawesome.com/releases/v5.0.6/css/all.css' , array() );
        wp_enqueue_style( 'style.min.css', get_template_directory_uri() . '/css/style.min.css' , array('font-awesome.all.css') );

        //js読み込み
        wp_enqueue_script( 'jquery-3.3.1.min.js', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(),'20181030',true);
        wp_enqueue_script( 'bundle.min.js', get_template_directory_uri() . '/js/bundle.min.js', array('jquery-3.3.1.min.js'),'20181030',true);
        wp_enqueue_script( 'common_script.js', get_template_directory_uri() . '/js/common_script.js', array('bundle.min.js'),'20181030',true);

        if(is_front_page()){
            wp_enqueue_script( 'home.js', get_template_directory_uri() . '/js/home.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/about_us.php')){
            wp_enqueue_script( 'common_about_us.js', get_template_directory_uri() . '/js/common_about_us.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/about_us-message.php')){
            wp_enqueue_script( 'common_about_us.js', get_template_directory_uri() . '/js/common_about_us.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/about_us-timing.php')){
            wp_enqueue_script( 'common_about_us.js', get_template_directory_uri() . '/js/common_about_us.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/about_us-prospectus.php')){
            wp_enqueue_script( 'common_about_us.js', get_template_directory_uri() . '/js/common_about_us.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/about_us-board_member.php')){
            wp_enqueue_script( 'common_about_us.js', get_template_directory_uri() . '/js/common_about_us.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/activity.php')){
            wp_enqueue_script( 'activity.js', get_template_directory_uri() . '/js/activity.js', array('common_script.js'),'20181030',true);
        }elseif( is_post_type_archive('report') || 'report' == get_post_type()){
            wp_enqueue_script( 'common_report.js', get_template_directory_uri() . '/js/common_report.js', array('common_script.js'),'20181030',true);
        }elseif( is_post_type_archive('seminar') || 'seminar' == get_post_type()){
            wp_enqueue_script( 'common_seminar.js', get_template_directory_uri() . '/js/common_seminar.js', array('common_script.js'),'20181030',true);
        }elseif( is_post_type_archive('book') || 'book' == get_post_type()){
            wp_enqueue_script( 'book.js', get_template_directory_uri() . '/js/book.js', array('common_script.js'),'20181030',true);
        }elseif(is_page_template('page-template/contact.php')){
        }elseif(is_page_template('page-template/link.php')){
        }elseif(is_page_template('page-template/privacy.php')){
        }elseif(is_page_template('page-template/sitemap.php')){
        }elseif(is_404()){
        }

    }
    add_action( 'wp_enqueue_scripts', 'lazak_scripts_styles' );

/**
 * bodyid
 *
 * @return void
 */

    function get_body_id($echo=false) {

        $ret = 'home';

        if(is_front_page()){
            $ret = 'home';
        }elseif(is_page_template('page-template/about_us.php')){
            $ret = 'about_us';
        }elseif(is_page_template('page-template/about_us-message.php')){
            $ret = 'message';
        }elseif(is_page_template('page-template/about_us-timing.php')){
            $ret = 'timing';
        }elseif(is_page_template('page-template/about_us-prospectus.php')){
            $ret = 'prospectus';
        }elseif(is_page_template('page-template/about_us-board_member.php')){
            $ret = 'board_member';
        }elseif(is_page_template('page-template/activity.php')){
            $ret = 'activity';
        }elseif( is_post_type_archive('report') || 'report' == get_post_type()){
            if(is_tax('report_category')){
                $ret = 'report_archive';
            }elseif(is_single()){
                $ret = 'report_single';
            }else{
                $ret = 'report';
            }
        }elseif( is_post_type_archive('seminar') || 'seminar' == get_post_type()){
            if(is_tax('seminar_category')){
                $ret = 'seminar_archive';
            }else{
                $ret = 'seminar';
            }
        }elseif( is_post_type_archive('book') || 'book' == get_post_type()){
            $ret = 'book';
        }elseif(is_page_template('page-template/contact.php')){
            $ret = 'contact';
        }elseif(is_page_template('page-template/link.php')){
            $ret = 'link';
        }elseif(is_page_template('page-template/privacy.php')){
            $ret = 'privacy';
        }elseif(is_page_template('page-template/sitemap.php')){
            $ret = 'sitemap';
        }elseif(is_404()){
            $ret = 'notfound';
        }

        if($echo){
            echo $ret;
        }else{
            return $ret;
        }
    }

    function get_seminar_date($date,$time){

        $ret = '';
        $week = array( "日", "月", "火", "水", "木", "金", "土" );
        $datetime = $date;

        if($time){
           $datetime .= ' ' . $time;
           $ret = new DateTime($datetime);
           if($ret->format('a') === 'am'){
                $ampm = '午前';
           }else{
                $ampm = '午後';
           }
           return $ret->format('Y年m月d日') . '(' . $week[$ret->format('w')] . ') ' . $ampm . $ret->format('h時i分') . '～';
        }else{
           $ret = new DateTime($datetime);
           return $ret->format('Y年m月d日') . '(' . $week[$datetime->format('w')] . ')';
        }
    }


    function get_seminar_data($term_slug){

        $ret = array();

        $Data = get_posts(array(
            'posts_per_page'   => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'seminar_category',
                    'field' => 'slug',
                    'terms' => $term_slug
                )
            ),
            'orderby' => 'meta_value',
            'meta_key' => 's_date_for_query',
            'order' => 'DESC',
            'post_type'  => 'seminar'
        ));
        //年ごとにまとめる
        if(!empty($Data)){
            foreach($Data as $post){
                setup_postdata($post);
                $tmp = array();
                $dateData = explode('-' , get_field('s_date_for_query',$post->ID));
                $year = $dateData[0];
                $tmp['id'] =  $post->ID;
                $tmp['title'] = get_the_title($post->ID);
                $tmp['date'] =  get_field('s_date',$post->ID);
                $tmp['place'] =  get_field('s_place',$post->ID);
                $tmp['theme'] =  get_field('s_theme',$post->ID);
                $ret[$year][] = $tmp;
            }
        }
        return $ret;
    }

    function get_past_seminar_data($term=''){

        $ret = array();

        if($term){
            $Data = get_posts(array(
                'posts_per_page'   => -1,
                'meta_query' => array(
                    array(
                        'key' => 's_date_for_query',
                        'value' =>date("Y-m-d"),
                        'compare' => '<',
                        'type'=>'DATE'
                    )
                ),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'seminar_category',
                        'field' => 'slug',
                        'terms' => $term
                    )
                ),
                'orderby' => 'meta_value',
                'meta_key' => 's_date_for_query',
                'order' => 'DESC',
                'post_type'  => 'seminar'
            ));

        }else{
            $Data = get_posts(array(
                'posts_per_page'   => -1,
                'meta_query' => array(
                    array(
                        'key' => 's_date_for_query',
                        'value' =>date("Y-m-d"),
                        'compare' => '<',
                        'type'=>'DATE'
                    )
                ),
                'orderby' => 'meta_value',
                'meta_key' => 's_date_for_query',
                'order' => 'DESC',
                'post_type'  => 'seminar'
            ));
        }
        //年ごとにまとめる
        if(!empty($Data)){
            foreach($Data as $post){
                setup_postdata($post);
                $tmp = array();
                $dateData = explode('-' , get_field('s_date_for_query',$post->ID));
                $year = $dateData[0];
                $tmp['id'] =  $post->ID;
                $tmp['title'] = get_the_title($post->ID);
                $tmp['date'] =  get_field('s_date',$post->ID);
                $tmp['place'] =  get_field('s_place',$post->ID);
                $tmp['theme'] =  get_field('s_theme',$post->ID);
                $ret[$year][] = $tmp;
            }
        }
        return $ret;
    }