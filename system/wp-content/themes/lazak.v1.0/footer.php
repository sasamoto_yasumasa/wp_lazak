<!-- /#wrap --></div>
<footer id="footer">
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div id="ft_logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ft_logo01.png" alt="LAZAK 在日コリアン弁護士協会" width="212" height="49"></a>
                    </div>
                </div>
                <div class="col-9">
                    <div id="ft_link" class="clearfix">
                        <ul>
                            <li>
                                <a href="<?php echo esc_url(home_url('/about_us/')); ?>">LAZAKについて</a>
                                <ul>
                                    <li><a href="<?php echo esc_url(home_url('/about_us/message/')); ?>">代表挨拶</a></li>
                                    <li><a href="<?php echo esc_url(home_url('/about_us/timing/')); ?>">設立の契機</a></li>
                                    <li><a href="<?php echo esc_url(home_url('/about_us/prospectus/')); ?>">設立趣意書</a></li>
                                    <li><a href="<?php echo esc_url(home_url('/about_us/board_member/')); ?>">役員紹介</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li><a href="<?php echo esc_url(home_url('/activity/')); ?>">LAZAKの取組み</a></li>
                            <li><a href="<?php echo esc_url(home_url('/report/')); ?>">活動報告</a></li>
                            <li><a href="<?php echo esc_url(home_url('/seminar/')); ?>">行事予定</a></li>
                            <li><a href="<?php echo esc_url(home_url('/book/')); ?>">書籍・出版物</a></li>
                        </ul>
                        <ul>
                            <li><a href="<?php echo esc_url(home_url('/contact/')); ?>">お問合せ</a></li>
                            <li><a href="<?php echo esc_url(home_url('/privacy/')); ?>">プライバシーポリシー</a></li>
                            <li><a href="<?php echo esc_url(home_url('/sitemap/')); ?>">サイトマップ</a></li>
                            <li><a href="<?php echo esc_url(home_url('/link/')); ?>">リンク集</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <small>Copyright &copy; LAZAK. All Rights Reserved.</small>
<!-- /#footer --></footer>
<div id="pagetop" style="">
    <img src="<?php echo get_template_directory_uri(); ?>/images/common/btn_top01.png" alt="ページの上部へ">
</div>
    <?php wp_footer(); ?>
</body>
</html>