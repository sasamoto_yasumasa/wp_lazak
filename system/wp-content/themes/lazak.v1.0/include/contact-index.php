<?php
  global $checkArr,$setHidden;
?>
    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>お問合せ</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1>
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/contact/tit01.jpg" alt="お問合せ">
                        </h1>
                        <div id="c01">
                            <div class="lead_text">
                                <p>以下のフォームより、お問合せを承っております。<br>
                                必要事項をご入力の上、プライバシーポリシーに同意を頂けましたら、「同意して確認画面へ」ボタンをクリックして下さい。<br>
                                なお、こちらのお問合せでは法律相談や弁護士紹介は一切行っておりません。予めご了承下さい。</p>
                            </div>
                            <div class="form_wrap">
                                <form action="" method="post" accept-charset="utf-8">
                                    <input type="hidden" name="mode" value="check" >

                                    <?php if(isset($checkArr["isError"]) && $checkArr["isError"]): ?>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="p-3 mb-5 bg-danger text-white">入力内容をご確認ください</div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="att01">
                                        <p>※【必須】項目は必ずご入力下さい。</p>
                                    </div>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th class="req">お名前</th>
                                                <td>
                                                    <div class="input_wrap">
                                                        <input type="text" id="username" name="username" value="<?php echo hsc($_POST['username']); ?>">
                                                    </div>
                                                    <div class="assist">
                                                        <span>（入力例）山田太郎</span>
                                                    </div>
                                                    <?php if(isset($checkArr["message"]["username"])): ?>
                                                        <div class="text-danger"><?php echo $checkArr["message"]["username"]; ?></div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="req">お名前（フリガナ）</th>
                                                <td>
                                                    <div class="input_wrap">
                                                        <input type="text" id="username_furigana" name="username_furigana" value="<?php echo hsc($_POST['username_furigana']); ?>">
                                                    </div>
                                                    <div class="assist">
                                                        <span>（入力例）ヤマダタロウ</span>
                                                    </div>
                                                    <?php if(isset($checkArr["message"]["username_furigana"])): ?>
                                                        <div class="text-danger"><?php echo $checkArr["message"]["username_furigana"]; ?></div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="req">メールアドレス</th>
                                                <td>
                                                    <div class="input_wrap">
                                                        <input type="text" id="mail1" name="mail1" value="<?php echo hsc($_POST['mail1']); ?>">
                                                    </div>
                                                    <div class="assist">
                                                        <span>（入力例）yamada@sample.com</span>
                                                    </div>
                                                    <?php if(isset($checkArr["message"]["mail1"])): ?>
                                                        <div class="text-danger"><?php echo $checkArr["message"]["mail1"]; ?></div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="req">メールアドレス(確認)</th>
                                                <td>
                                                    <div class="input_wrap">
                                                        <input type="text" id="mail2" name="mail2" value="<?php echo hsc($_POST['mail2']); ?>">
                                                    </div>
                                                    <div class="assist">
                                                        <span>（入力例）yamada@sample.com</span>
                                                    </div>
                                                    <?php if(isset($checkArr["message"]["mail2"])): ?>
                                                        <div class="text-danger"><?php echo $checkArr["message"]["mail2"]; ?></div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>お電話番号</th>
                                                <td>
                                                    <div class="input_wrap">
                                                        <input type="text" id="tel1" name="tel1" value="<?php echo hsc($_POST['tel1']); ?>">
                                                    </div>
                                                    <div class="assist">
                                                        <span>（入力例）0698761234</span>
                                                    </div>
                                                    <?php if(isset($checkArr["message"]["tel1"])): ?>
                                                        <div class="text-danger"><?php echo $checkArr["message"]["tel1"]; ?></div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="req">お問合せ内容</th>
                                                <td>
                                                    <div class="input_wrap">
                                                        <textarea name="message"  rows="8" cols="80"><?php echo hsc($_POST['message']); ?></textarea>
                                                    </div>
                                                    <?php if(isset($checkArr["message"]["message"])): ?>
                                                        <div class="text-danger"><?php echo $checkArr["message"]["message"]; ?></div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="c01_01">
                                        <div class="privacy">
                                            <div class="inner">
                                                <div class="privacy_tit">
                                                    プライバシーポリシー
                                                </div>
                                                <div class="summary">
                                                    <p>在日コリアン弁護士協会（以下「当協会」といいます）は、個人情報の取扱に関し、個人情報の保護に関する法律(以下｢個人情報保護法｣といいます)、その他個人情報の取扱について定められた関係法令を遵守するとともに、以下のプライバシーポリシーを定め、その適切な取扱に努めます。</p>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">1. 個人情報の取得</div>
                                                    <div class="summary">
                                                        <p>当協会は、個人情報を利用目的の達成に必要な範囲で、適正な手段により取得します。</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">2. 個人情報の利用目的</div>
                                                    <div class="summary">
                                                        <p>当協会は、個人情報を、以下の目的で利用します。<br>
                                                        個人情報保護法その他の法令により認められる事由がある場合を除き、ご本人の同意がない限り、この範囲を超えて個人情報を利用することはありません。</p>
                                                        <ul class="list_style04">
                                                            <li>当協会の業務の適切かつ円滑な遂行</li>
                                                            <li>その他、上記の利用目的に付随する事項の遂行</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">3. 個人情報の第三者提供</div>
                                                    <div class="summary">
                                                        <p>当協会は、個人情報の第三者提供については、個人情報保護法、その他の法令を遵守します。</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">4. 個人情報の管理</div>
                                                    <div class="summary">
                                                        <p>当協会は、個人情報の第三者提供については、個人情報保護法、その他の法令を遵守します。</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">5. 個人情報の開示、訂正等のお求め</div>
                                                    <div class="summary">
                                                        <p>当協会は、個人情報について、個人情報保護法に基づく開示、訂正等ご本人からのお申出があった場合には、個人情報保護法に従い、適切に対応いたします。<br>
                                                        詳しくは、以下のお問合せ先までご連絡ください。</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">6. お問合せ窓口</div>
                                                    <div class="summary">
                                                        <p>当協会の個人情報の取扱に関するご意見、ご質問、開示等のご請求、その他個人情報の取扱に関するお問合わせの窓口は、当協会といたします。</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="tit_style01">7. 本プライバシーポリシーの継続的改善</div>
                                                    <div class="summary">
                                                        <p>当協会は、個人情報の取扱に関する運用状況を適宜見直すとともに、継続的な改善に努めます。必要に応じて、プライバシーポリシーを変更することがあります。</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="c01_02" class="btn_wrap">
                                        <p>上記プライバシーポリシーにご同意頂きましたら、お問合せ内容の確認画面へお進み下さい。</p>
                                        <div class="row justify-content-center">
                                            <div class="col-6">
                                                <div id="to_confirm_btn" class="link_style06">同意して確認画面へ</div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
