<?php
  global  $ticket,$setHidden;
?>

    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>お問合せ</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1>
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/contact/tit01.jpg" alt="お問合せ">
                        </h1>
                        <div id="c01">
                            <div class="lead_text">
                                <p>ご入力内容を修正される場合は「入力内容の修正」ボタンを、送信される場合は「送信」ボタンをクリックして下さい。</p>
                            </div>
                            <div class="form_wrap confirm">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th>お名前</th>
                                                <td><?php echo hsc($_POST['username']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>お名前（フリガナ）</th>
                                                <td><?php echo hsc($_POST['username_furigana']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>メールアドレス</th>
                                                <td><?php echo hsc($_POST['mail1']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>お電話番号</th>
                                                <td><?php echo hsc($_POST['tel1']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>お問合せ内容</th>
                                                <td><?php echo nl2br(hsc($_POST['message'])); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="c01_02" class="btn_wrap">
                                        <div class="row justify-content-center">
                                            <div class="col-4">
                                                <div id="to_index_btn" class="link_style07" >入力内容の修正</div>
                                            </div>
                                            <div id="to_send_btn" class="col-4">
                                                <div class="link_style06" >送信</div>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="edit_form" name="edit_form" action="" method="post">
                                        <?php echo $setHidden; ?>
                                        <input type="hidden" name="mode" value="edit">
                                    </form>
                                    <form id="send_form" name="send_form" action="" method="post">
                                        <?php echo $setHidden; ?>
                                        <input type="hidden" name="mode" value="send">
                                        <input type="hidden" name="ticket" value="<?php echo $ticket ;?>">
                                    </form>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
