<?php

//バリデーション
function run_validate_inquiry(){
    mb_regex_encoding("UTF-8");

    $retArr = array();
    $retArr["isError"] = false;

    if(!exist_check($_POST['username'])){
        $retArr["isError"]=true;
        $retArr["message"]["username"] = "お名前をご入力ください。";
    }

    if(!exist_check($_POST['username_furigana'])){
        $retArr["isError"]=true;
        $retArr["message"]["username_furigana"] = "お名前（フリガナ）をご入力ください。";
    }else{
        if(!mb_ereg("^[ア-ン゛゜ァ-ォャ-ョー「」、　]+$", $_POST['username_furigana'])){
            $retArr["isError"]=true;
            $retArr["message"]["username_furigana"] = "お名前（フリガナ）は全角カタカナでご入力ください。";
        }
    }

    if(exist_check($_POST['tel1'])){
        if( !int_type_check($_POST['tel1'])){
            $retArr["isError"]=true;
            $retArr["message"]["tel1"] = "電話番号は半角をご入力ください。";
        }
    }

    if(!exist_check($_POST['mail1'])){
        $retArr["isError"]=true;
        $retArr["message"]["mail1"] = "メールアドレスをご入力ください。";
    }elseif(!email_type_check(($_POST['mail1']))){
        $retArr["isError"]=true;
        $retArr["message"]["mail1"] = "メールアドレスの書式をご確認ください。";
    }


    if(!exist_check($_POST['mail2'])){
        $retArr["isError"]=true;
        $retArr["message"]["mail2"] = "メールアドレス（確認）をご入力ください。";
    }elseif(!email_type_check(($_POST['mail2']))){
        $retArr["isError"]=true;
        $retArr["message"]["mail2"] = "メールアドレス（確認）の書式をご確認ください。";
    }elseif($_POST['mail1'] !== $_POST['mail2']){
        $retArr["isError"]=true;
        $retArr["message"]["mail2"] = "メールアドレスとメールアドレス（確認）が一致しません。";
    }

    if(!exist_check($_POST['message'])){
        $retArr["isError"]=true;
        $retArr["message"]["message"] = "お問合せ内容をご入力ください。";
    }


    return $retArr;
}




function common_strip_slashes_post($pArr){

        foreach ( $_POST as $name => $value )
        {
            if(is_array($value)){
                foreach($value as $item){
                    stripslashes(strip_tags($item));
                }
            }else{
                $_POST[$name] = stripslashes(strip_tags($value));
            }
        }

        foreach ($pArr as $key => $pname) {
            if(!isset($_POST[$pname])) $_POST[$pname] = "";
        }

}

function hsc($value){

    return (isset($value))? htmlspecialchars($value, ENT_QUOTES) : "" ;

}


function createHidden(){

    $retST="";

    foreach ( $_POST as $name => $value )
    {
        if($name == "mode"){continue;}

        if(is_array($value)){
            $value = join(',',$value);
        }
            $retST.='<input type="hidden" name="'.$name.'" value = "'.htmlspecialchars($value, ENT_QUOTES).'">';
    }
    return $retST;
}


function unsetPOST(){

    foreach ( $_POST as $name => $value )
    {
            $_POST[$name] = '';
    }

}


function setYear($target){
   $setST="";
     $endYear = intval(date('Y') - 20);
     $startYear = $endYear - 45;

   for($i=$startYear ; $i<=$endYear ; $i++){
        $setST.='<option value="'.$i.'"';

        if(intval($i)=== intval($target)){
            $setST.=" selected";
        }
        $setST.='>'.$i.'</option>';
   }
   return $setST;
}

function setMonth($target){

   $setST="";

   for($i=1 ; $i<=12 ; $i++){
        $setST.='<option value="'.$i.'"';

        if(intval($i)=== intval($target)){
            $setST.=" selected";
        }
        $setST.='>'.$i.'</option>';
   }
   return $setST;
}

function setDay($target){
   $setST="";

   for($i=1 ; $i<=31 ; $i++){
        $setST.='<option value="'.$i.'"';

        if(intval($i)=== intval($target)){
            $setST.=" selected";
        }
        $setST.='>'.$i.'</option>';
   }
   return $setST;

}

function setSelector($arr,$target,$xss=TRUE){

   $setST="";

   foreach($arr as $item){

       if($xss){
        $item=trim(htmlspecialchars(mb_convert_encoding($item, "UTF-8","auto")));
       }

        $setST.='<option value="'.$item.'"';


        if($item === $target){

            $setST.='selected ';

        }
         $setST.='>'.$item.'</option>';

   }
   return $setST;
}


function setRadio($name,$arr,$target,$arg="",$xss=TRUE){

   $setST="";

   foreach($arr as $item){

       if($xss){
        $item=trim(htmlspecialchars(mb_convert_encoding($item, "UTF-8","auto")));
       }
        $setST.='<label class="db fl mr-25 ml-25 cf"><input type="radio" name="'.$name.'" value="'.$item.'" ';

        if($item === $target){
           $setST.='checked ';
        }
         $setST.= $arg.'><span class="">'.$item.'</span></label>';
   }
   return $setST;
}


function setCheckBox($name,$arr,$target,$xss=TRUE,$addBR=FALSE){


    $brArray=Array('');

   $setST="";

   foreach($arr as $item){

       if($xss){
        $item=trim(htmlspecialchars(mb_convert_encoding($item, "UTF-8","auto")));
       }
        $setST.='<li><label><input type="checkbox" name="'.$name.'" value="'.$item.'"';

        if(!is_array($target)){
            $target = explode(',', $target);
        }

        if(_isChecked($item,$target)){
           $setST.='checked ';
        }
         $setST.=" />\r\n".$item;
             $setST.="</label></li>\r\n";

        if($addBR){
            $setST.="<br />";
        }
            if(in_array($item,$brArray)){
                    $setST.="<br style='clear:both;' />";
            }

   }
   return $setST;
}

function _isChecked($item,$arr){

   if(is_array($arr)){
   foreach($arr as $val){
       if($item === $val){
           return true;
       }
   }
   }
   return false;
}


function createPref($target=false){

 $prefArray = array("北海道","青森県","岩手県","宮城県","秋田県","山形県","福島県","茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県","新潟県","富山県","石川県","福井県","山梨県","長野県","岐阜県","静岡県","愛知県","三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県","鳥取県","島根県","岡山県","広島県","山口県","徳島県","香川県","愛媛県","高知県","福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県");

    foreach($prefArray as $pref){
        echo '<option value="'.$pref.'"';

        if($target && $target == $pref){
            echo ' selected';
        }

        echo '>'.$pref.'</option>';
    }

}



    /**
 * 必須入力チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function exist_check_cus( $input )
{
    if ( !strlen($input) )
    {
        return false;
    }
    else
    {
        return true;
    }
}


    /**
 * 必須入力チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function exist_check( $input )
{
    if ( !$input )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * 必須選択チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function selected_check( $input )
{
    if ( ! $input )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * 文字列の個数チェック
 *
 * @param string $input 入力値
 * @param int    $length 文字の個数 ( MAX )
 * @return bool
 *
 */
function length_check( $input, $length )
{
    if ( ! $input )
    {
        return true;
    }
    else
    {
        if ( mb_strlen( $input,'UTF-8' ) > $length )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

/**
 * email 形式チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function email_type_check( $input )
{
    if (!$input)
    {
        return false;
    }

    if ( !preg_match( "/^[a-zA-Z0-9_\.\-]+@[^.]+\..+/", $input ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}



/**
 * 数値 形式チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function int_type_check( $input )
{
    if ( ! $input )
    {
        return true;
    }

    if ( ! is_numeric( $input ) )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * 電話番号 形式チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function tel_type_check( $input )
{
    if ( ! $input )
    {
        return true;
    }

    $arrInput = explode( "-", $input );

    if ( count( $arrInput ) != 3 )
    {
        return false;
    }

    if ( ! is_numeric( $arrInput[ 0 ] ) || strlen( $arrInput[ 0 ] ) > 5 || strlen( $arrInput[ 0 ] ) < 2 )
    {
        return false;
    }

    if ( ! is_numeric( $arrInput[ 1 ] ) || strlen( $arrInput[ 1 ] ) > 5 || strlen( $arrInput[ 1 ] ) < 2 )
    {
        return false;
    }

    if ( ! is_numeric( $arrInput[ 2 ] ) || strlen( $arrInput[ 2 ] ) != 4 )
    {
        return false;
    }

    return true;
}


function tel_type_check2($input){

    $input = mb_convert_kana( $input,"n");

    if ( ! $input )
    {
        return true;
    }


    if (preg_match("/^0[1-9][0-9]{0,3}-?[0-9]{1,4}-?[0-9]{4}$/", $input)) {
        return true;
    }else{
        return false;
    }
}


/**
 * 郵便番号 形式チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function zipcode_type_check( $input )
{
    if ( ! $input )
    {
        return true;
    }

    $arrInput = explode( "-", $input );

    if ( count( $arrInput ) != 2 )
    {
        return false;
    }

    if ( ! is_numeric( $arrInput[ 0 ] ) || strlen( $arrInput[ 0 ] ) != 3 )
    {
        return false;
    }

    if ( ! is_numeric( $arrInput[ 1 ] ) || strlen( $arrInput[ 1 ] ) != 4 )
    {
        return false;
    }

    return true;
}



/**
 * 半角英数 チェック
 *
 * @param string $input 入力値
 * @return bool
 *
 */
function alphanumeric_type_check( $input )
{
    if ( ! $input )
    {
        return true;
    }

    if( ! preg_match( "/^[a-zA-Z0-9]+$/" , $input ) )
    {
        return false;
    }

    return true;
}

function delSession(){
        global $_SESSION;
        $_SESSION = array();
        session_destroy();
}

//全角チェック
function is_zenkaku($str, $encoding = null) {
    if (is_null($encoding)) {
        $encoding = mb_internal_encoding();
    }
    $len = mb_strlen($str, $encoding);
    for ($i = 0; $i < $len; $i++) {
        $char = mb_substr($str, $i, 1, $encoding);
        if (is_hankaku($char, true, true, $encoding)) {
            return false;
        }
    }
    return true;
}

    //半角チェック
function is_hankaku($str, $include_kana = false, $include_controls = false, $encoding = null) {
    if (!$include_controls && !ctype_print($str)) {
        return false;
    }

    if (is_null($encoding)) {
        $encoding = mb_internal_encoding();
    }
    if ($include_kana) {
        $to_encoding = 'SJIS';
    } else {
        $to_encoding = 'UTF-8';
    }
    $str = mb_convert_encoding($str, $to_encoding, $encoding);

    if (strlen($str) === mb_strlen($str, $to_encoding)) {
        return true;
    } else {
        return false;
    }

}

