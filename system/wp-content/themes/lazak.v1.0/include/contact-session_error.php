    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>お問合せ</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1>
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/contact/tit01.jpg" alt="お問合せ">
                        </h1>
                        <div id="c01">
                            <div class="lead_text pb-3 text-center">
                                <p>
                                  <span class="font-weight-bold d-block pb-3">予期せぬエラーが発生しました</span>
                                  <span class="d-block pb-3">お手数ですが、時間をおいてから再度お試しください。</span>
                                  <span class="d-block pb-2">【エラーの主な原因】</span>
                                  <span class="d-block pb-1">不正なボタン操作。</span>
                                  <span class="d-block pb-1">Cookie（クッキー）の無効化。</span>
                                  <span class="d-block pb-1">複数画面からの同時アクセス。</span>
                                  <span class="d-block pb-1">その他、予期せぬシステムの不具合。</span>
                                </p>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    <a class="link_style07" href="<?php echo esc_url(home_url('/')); ?>">HOMEに戻る</a>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>