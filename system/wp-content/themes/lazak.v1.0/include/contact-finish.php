    <div id="content_wrapper" class="one_column">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div id="breadcrumb">
                        <ul class="clearfix">
                            <li><a href="<?php echo esc_url(home_url('/')); ?>">HOME</a></li>
                            <li>お問合せ</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-8">
                    <main id="primary">
                        <h1>
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/contact/tit01.jpg" alt="お問合せ">
                        </h1>
                        <div id="c01">
                            <div class="lead_text">
                                <p>
                                <span class="font-weight-bold d-block pb-3">お問合せを承りました。</span>
                                追って、担当者からご連絡差し上げます。連絡がない場合はシステムの不具合等の可能性がございます。その際はご容赦ください。</p>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-4">
                                    <a class="link_style07" href="<?php echo esc_url(home_url('/')); ?>">HOMEに戻る</a>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
